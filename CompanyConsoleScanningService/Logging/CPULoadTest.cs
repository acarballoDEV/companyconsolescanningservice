﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Logging
{
    public static class CPULoadTest 
    {
        private static int count { get; set; }

      
        public static async Task CPULoadRun()  
        {
            
            count = 0;
            int ThreadNumber = Convert.ToInt32(ConfigurationManager.AppSettings["ThreadNumber"].ToString());
            int LongThreadThreadNumber = Convert.ToInt32(ConfigurationManager.AppSettings["LongThreadThreadNumber"].ToString());
            Int64 ShortRunningCounter = Convert.ToInt64(ConfigurationManager.AppSettings["ShortRunningCounter"].ToString());
            Int64 LongRunningCounter = Convert.ToInt64(ConfigurationManager.AppSettings["LongRunningCounter"].ToString());
            Int32 maxConcurrency = Convert.ToInt32(ConfigurationManager.AppSettings["maxConcurrency"].ToString());
          
            var messages = new List<string>();
            using (SemaphoreSlim concurrencySemaphore = new SemaphoreSlim(maxConcurrency))
            {
                List<Task> tasks = new List<Task>();

                Action<Action> measure = (body) =>
                {
                    var starttime = DateTime.Now;
                    body();
                    count++;
                    Logging.Log.LogInfoLevel(String.Format("Task {0}  - ThreadId {1}, Elab Time {2}", count, Thread.CurrentThread.ManagedThreadId, DateTime.Now - starttime));
                    //Console.WriteLine("{0} {1}", DateTime.Now - starttime, Thread.CurrentThread.ManagedThreadId);
                };


                Action calcJob = null;
                calcJob = () =>
                {
                    for (int i = 0; i < ShortRunningCounter; i++)
                    {

                    }
                };

                Action calcLongJob = null;
                calcLongJob = () =>
                {
                    for (int i = 0; i < LongRunningCounter; i++)
                    {

                    }
                };

                measure(() =>
                {
                    List<Task> ShortRunningTaskList = new List<Task>();
                    List<Task> LongRunningTaskList = new List<Task>();

                    for (int i = 0; i < ThreadNumber; i++)
                    {
                        concurrencySemaphore.Wait();

                        var t = Task.Factory.StartNew(() =>
                        {

                            try
                            {
                                ShortRunningTaskList.Add(Task.Factory.StartNew(() => measure(calcJob)));
                            }
                            finally
                            {
                                concurrencySemaphore.Release();
                            }
                        });

                        tasks.Add(t);
                        
                    }
                    for (int i = 0; i < LongThreadThreadNumber; i++)
                    {
                        concurrencySemaphore.Wait();

                        var t = Task.Factory.StartNew(() =>
                        {

                            try
                            {
                                LongRunningTaskList.Add(Task.Factory.StartNew(() => measure(calcLongJob), TaskCreationOptions.LongRunning));
                            }
                            finally
                            {
                                concurrencySemaphore.Release();
                            }
                        });

                        tasks.Add(t);
                        
                    }

                    //Task.WaitAll(ShortRunningTaskList.ToArray());
                    //Task.WaitAll(LongRunningTaskList.ToArray());
                }
                );

                await Task.Delay(100);
            }

   
        }

    }
}
