﻿namespace ES.CompanyConsole.Models.Events
{
    public enum LogTraceAndEventSeverityLevels
    {
        Info,
        Warning,
        Error
    }

    public interface ILogTraceAndEventService
    {
        void LogTraceAndEvent(object sender, LogTraceAndEventSeverityLevels logTraceAndEventSeverityLevel,
            string content);
    }
}
