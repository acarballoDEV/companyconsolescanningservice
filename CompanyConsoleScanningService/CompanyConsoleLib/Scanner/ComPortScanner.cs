﻿using System;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Management;
using System.Threading;
using System.Threading.Tasks;
using ES.CompanyConsole.Models.Composition;
using ES.CompanyConsole.Models.Events;


namespace ES.CompanyConsole.Models.Scanners
{
    public class ComPortScanner : IComPortScannerService, ILogTraceAndEventService, ICustomModule
    {
        private readonly SerialPort _comPort = new SerialPort();
        private readonly ILogTraceAndEventService _logTraceAndEventService;
        private CancellationTokenSource _cancellationTokenSource;

        private string _comPortBuffer;
        private bool _comPortClose;
        private Stream _comPortStream;

        public ComPortScanner(object sender)
        {
            if (sender != null)
            {
                _logTraceAndEventService = (ILogTraceAndEventService) sender;
            }
        }

        public event EventHandler<BarcodeEventArgs> BarcodeReceived;

        public void Open()
        {
            try
            {
                if (!bool.Parse(ConfigurationManager.AppSettings["Com1Enabled"])) return;
                _comPort.PortName = "COM" + int.Parse(ConfigurationManager.AppSettings["Com1PortNr"]);
                var com1StopBits = Enum.GetName(typeof (StopBits), int.Parse(ConfigurationManager.AppSettings["Com1StopBits"]));
                if (com1StopBits != null)
                {
                    _comPort.StopBits = (StopBits) Enum.Parse(typeof (StopBits), com1StopBits);
                }
                else
                {
                    _comPort.StopBits = StopBits.One;
                    LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Error,
                        "COM1 StopBits - Default value used.");
                }
                _comPort.DataBits = int.Parse(ConfigurationManager.AppSettings["Com1DataBits"]);
                _comPort.BaudRate = int.Parse(ConfigurationManager.AppSettings["Com1BaudRate"]);
                var com1Parity = Enum.GetName(typeof (Parity), int.Parse(ConfigurationManager.AppSettings["Com1Parity"]));
                if (com1Parity != null)
                {
                    _comPort.Parity = (Parity) Enum.Parse(typeof (Parity), com1Parity);
                }
                else
                {
                    _comPort.Parity = Parity.None;
                    LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Error,
                        "COM1 Parity - Default value used.");
                }
                 
                
                _comPortClose = false;
                _comPort.DataReceived += ComPort_DataReceived;
                _comPort.ErrorReceived += ComPort_ErrorReceived;
                _cancellationTokenSource = new CancellationTokenSource();
                var token = _cancellationTokenSource.Token;

                var comPortReaderTask = Task.Factory.StartNew(() => { ComPortReader(token); }, token,
                    TaskCreationOptions.LongRunning, TaskScheduler.Default);

                try
                {
                    comPortReaderTask.Wait(token);
                }
                catch (AggregateException aex)
                {
                    if (aex.InnerExceptions != null)
                    {
                        foreach (var innerException in aex.InnerExceptions)
                        {
                            LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Error, innerException?.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(this, nameof(Open), ex, default(string));
            }
        }

        public void Close()
        {
            try
            {
                _comPortClose = true;

                if (_comPort != null)
                {
                    if (_comPort.IsOpen)
                    {
                        _comPort.Close();
                        try
                        {
                            GC.ReRegisterForFinalize(_comPortStream);
                        }
                        catch (Exception ex)
                        {
                            LogException(this, nameof(Close), ex, default(string));
                        }
                    }
                }

                _cancellationTokenSource?.Cancel();
            }
            catch (Exception ex)
            {
                LogException(this, nameof(Close), ex, default(string));
            }
        }

        public void LogException(object sender, string methodName, Exception ex, string info)
        {
            //_logExceptionService?.LogException(sender, methodName, ex, info);
        }

        public void LogTraceAndEvent(object sender, LogTraceAndEventSeverityLevels logTraceAndEventSeverityLevel,
            string content)
        {
            _logTraceAndEventService?.LogTraceAndEvent(sender, logTraceAndEventSeverityLevel, content);
        }

        private void ComPortReader(CancellationToken cancellationToken)
        {
            while (!_comPortClose)
            {
                try
                {
                    _comPort.Open();
                }
                catch (IOException ioEx1)
                {
                    LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Error, ioEx1.Message);
                    _comPort.PortName = "COM" + ComPortAutoDetect();
                }

                catch (Exception ioEx2)
                {
                    LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Error, ioEx2.Message);
                    return;
                }

                if (_comPort.IsOpen)
                {
                    _comPortStream = _comPort.BaseStream;
                    GC.SuppressFinalize(_comPortStream);
                    break;
                }
                Task.Delay(1000, cancellationToken).Wait(cancellationToken);

                if (cancellationToken.IsCancellationRequested)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                }
            }
        }

        protected virtual void OnBarcodeReceived(string barcode)
        {
            try
            {
                BarcodeReceived?.Invoke(this, new BarcodeEventArgs {Barcode = barcode});
            }
            catch (Exception ex)
            {
                LogException(this, nameof(OnBarcodeReceived), ex, default(string));
            }
        }

        private void ComPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                const char endOfMsgChar = (char) 13;
                const char newLineChar = (char) 10;

                _comPortBuffer = _comPortBuffer + _comPort.ReadExisting();
                if (_comPortBuffer.IndexOf(endOfMsgChar) <= 0) return;
                var barcode = _comPortBuffer.Substring(0, _comPortBuffer.IndexOf(endOfMsgChar));
                if (barcode.Contains(Convert.ToString(newLineChar)))
                {
                    barcode = barcode.Trim(newLineChar);
                }
                OnBarcodeReceived(barcode);
                _comPortBuffer = string.Empty;
            }
            catch (Exception ex)
            {
                LogException(this, nameof(ComPort_DataReceived), ex, default(string));
            }
        }

        private void ComPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            try
            {
                LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Error,
                    Enum.GetName(typeof (SerialError), e.EventType));
                ComPortReset();
            }
            catch (Exception ex)
            {
                LogException(this, nameof(ComPort_ErrorReceived), ex, default(string));
            }
        }

        public void ComPortReset()
        {
            try
            {
                Close();
                Open();
            }
            catch (Exception ex)
            {
                LogException(this, nameof(ComPortReset), ex, default(string));
            }
        }

        public int ComPortAutoDetect()
        {
            try
            {
                var comPort = 0;

                var moSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort");
                var moReturnCollection = moSearcher.Get();

                foreach (var mo in moReturnCollection)
                {
                    var name = mo["Name"].ToString().ToLower();

                    var index = name.LastIndexOf("(com", StringComparison.Ordinal);

                    if (index > 0 && name.Length > index + 5)
                    {
                        try
                        {
                            comPort = Convert.ToInt16(name.Substring(index + 4, name.Length - index - 5));
                        }
                        catch (Exception)
                        {
                            return 0;
                        }

                        if (IsVendorOk(name))
                        {
                            return comPort;
                        }
                    }

                    var ports = SerialPort.GetPortNames();
                    if (ports.Length <= 0) continue;
                    foreach (var port in ports)
                    {
                        var newPort = 0;
                        if (port.Length == 4)
                        {
                            try
                            {
                                newPort = Convert.ToInt16(port.Substring(3, 1));
                            }
                            catch (Exception)
                            {
                                return 0;
                            }
                        }

                        if (newPort != comPort)
                        {
                            return newPort;
                        }
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                LogException(this, nameof(ComPortAutoDetect), ex, default(string));
                return 0;
            }
        }

        private bool IsVendorOk(string value)
        {          
            return true;
        }
    }
}