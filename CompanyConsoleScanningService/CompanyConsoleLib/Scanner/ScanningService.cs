﻿using System;
using ES.CompanyConsole.Models.Composition;
using ES.CompanyConsole.Models.Events;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Configuration;
using System.Net;

namespace ES.CompanyConsole.Models.Scanners
{


    public enum BarcodeErrorTypes
    {
        NotDetected
    }

    public class ScanningService :  ILogTraceAndEventService, ICustomModule
    {
        private readonly ComPortScanner _comPortScanner1;
        private readonly ILogTraceAndEventService _logTraceAndEventService;
        private readonly String _previousBarcode = String.Empty;
        private string chromeExePath = ConfigurationManager.AppSettings["ChromeExePath"];
        private string pDFReaderExePath = ConfigurationManager.AppSettings["PDFReaderExePath"];
        private string dwgViewerExePath = ConfigurationManager.AppSettings["DwgViewerExePath"];

        public ScanningService(object sender)
        {
            if (sender != null)
            {
                _logTraceAndEventService = (ILogTraceAndEventService) sender;
            }
         
            _comPortScanner1 = new ComPortScanner(this);
            _comPortScanner1.BarcodeReceived += OnBarcodeReceived;       
        }

        public string Barcode
        {
            get { return _previousBarcode; }
            set { BarcodeScanned(value); }
        }

        public void Open()
        {
            try
            {
                _comPortScanner1.Open();
            }
            catch (Exception ex)
            {
                LogException(this, nameof(Open), ex, default(string));
            }
        }

        public void Close()
        {
            try
            {
                _comPortScanner1.Close();
            }
            catch (Exception ex)
            {
                LogException(this, nameof(Close), ex, default(string));
            }
        }

        public void LogException(object sender, string methodName, Exception ex, string info)
        {
            Logging.Log.LogFatalLevel(String.Format("{1},{2} : {0}", Logging.Log.GetExException(ex),sender.ToString(), methodName));
        }

        public void LogTraceAndEvent(object sender, LogTraceAndEventSeverityLevels logTraceAndEventSeverityLevel,
            string content)
        {
            switch (logTraceAndEventSeverityLevel)
            {
                case LogTraceAndEventSeverityLevels.Error:
                    Logging.Log.LogFatalLevel(String.Format("{1} : {0}", content, sender.ToString()));
                    break;
                case LogTraceAndEventSeverityLevels.Warning:
                    Logging.Log.LogWarningLevel(String.Format("{1} : {0}", content, sender.ToString()));
                    break;
                case LogTraceAndEventSeverityLevels.Info:
                    Logging.Log.LogInfoLevel(String.Format("{1} : {0}", content, sender.ToString()));
                    break;
            }
        }


        private void OnBarcodeReceived(object sender, BarcodeEventArgs e)
        {
            //ScannerNr is available in the BarcodeEventArgs if needed.
            Barcode = e.Barcode;
        }

        private void BarcodeScanned(string barcode)
        {
            try
            {
                LogTraceAndEvent(this, LogTraceAndEventSeverityLevels.Info, String.Format("Barcode {0} scanned", barcode));
                BarcodeAccepted(barcode);
            }
            catch (Exception ex)
            {
                LogException(this, nameof(BarcodeScanned), ex, default(string));
            }
        }

        private void BarcodeAccepted(string barcode)
        {
            string exePath = String.Empty;
            bool isUrlPath = false;

            try
            {
                isUrlPath = barcode.StartsWith("http");

                if (isUrlPath==true)
                {
                    try
                    {
                        if (!IsUrlExist(barcode))
                            throw new FileNotFoundException();
                    }

                    catch (FileNotFoundException e)
                    {
                        LogException(this, nameof(BarcodeAccepted), e, default(string));
                        return;
                    }

                }
                else { 
                    try
                    {
                        if (!File.Exists(barcode))
                            throw new FileNotFoundException();
                    }

                    catch (FileNotFoundException e)
                    {
                        LogException(this, nameof(BarcodeAccepted), e, default(string));
                        return;
                    }
                }

                string extension = barcode.Substring(barcode.Length - 3, 3);

                switch (extension)
                {
                    case "pdf":
                        exePath = pDFReaderExePath;
                        break;
                    case "dwg":
                    case "dwf":
                        exePath = dwgViewerExePath;
                        break;
                    default:
                        exePath = chromeExePath;
                        break;
                }

                ProcessStartInfo info = new ProcessStartInfo(exePath);

                switch (extension)
                {
                    case "pdf":
                        info.Arguments = String.Format("{0}{1}{2}", "/A \"", barcode, "\"");
                        break;
                    case "dwg":
                    case "dwf":
                        info.Arguments = barcode;
                        info.Arguments = String.Format("{0}{1}{2}", "/p \"", barcode, "\"");
                        break;
                    default:
                        info.Arguments = barcode;
                        break;
                }

                info.CreateNoWindow = true;
                info.UseShellExecute = false;
                Process.Start(info);

            }
            catch (Exception ex)
            {
                LogException(this, nameof(BarcodeAccepted), ex, default(string));
            }

        }

        public bool IsUrlExist(string url, int timeOutMs = 1000)
        {
            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Method = "HEAD";
            webRequest.Timeout = timeOutMs;

            try
            {
                var response = webRequest.GetResponse();
                /* response is `200 OK` */
                response.Close();
            }
            catch
            {
                /* Any other response */
                return false;
            }

            return true;
        }
    }
}