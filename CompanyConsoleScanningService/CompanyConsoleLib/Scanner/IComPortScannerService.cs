﻿using System;

namespace ES.CompanyConsole.Models.Scanners
{
    public interface IComPortScannerService
    {
        event EventHandler<BarcodeEventArgs> BarcodeReceived;
    }
}
