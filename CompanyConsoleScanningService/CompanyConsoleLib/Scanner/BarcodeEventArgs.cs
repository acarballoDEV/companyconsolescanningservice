﻿using System;

namespace ES.CompanyConsole.Models.Scanners
{
    public class BarcodeEventArgs : EventArgs
    {
        public string Barcode { get; set; }
    }
}
