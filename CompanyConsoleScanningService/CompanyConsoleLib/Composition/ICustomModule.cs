﻿namespace ES.CompanyConsole.Models.Composition
{
    public interface ICustomModule
    {
        void Open();
        void Close();
    }
}
