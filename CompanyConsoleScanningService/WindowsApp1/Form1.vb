﻿Imports ES.CompanyConsole.Models.Scanners
Imports ES.CompanyConsole.Models.Events
Public Class Form1
    Implements ILogTraceAndEventService

    Private _scanningService As ScanningService

    Public ReadOnly Property ScanningService As ScanningService
        Get
            Return _scanningService
        End Get
    End Property

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    Private Sub frm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        Try
            _scanningService = New ScanningService(Me)
            _scanningService.Open()

        Catch Ex As Exception
            'Exception
        End Try
    End Sub

    Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Closing
        'Try
        Try
            _scanningService.Close()

        Catch Ex As Exception
            'Exception
        End Try
    End Sub

    Public Sub LogTraceAndEvent(sender As Object, logTraceAndEventSeverityLevel As LogTraceAndEventSeverityLevels, content As String) Implements ILogTraceAndEventService.LogTraceAndEvent

    End Sub
End Class
